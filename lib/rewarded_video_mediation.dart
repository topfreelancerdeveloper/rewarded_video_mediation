library rewarded_video_mediation;

import 'dart:io';

import 'package:adcolony/adcolony.dart';
import 'package:admob_firebase_new_apis/admob_firebase_new_apis.dart';
import 'package:facebook_rewarded_video/facebook_rewarded_video.dart';
/* import 'package:facebook_audience_network/facebook_audience_network.dart'; */
import 'package:flutter/foundation.dart';
import 'package:flutter_mopub/flutter_mopub.dart';
import 'package:inmobi_rewarded_video/inmobi_rewarded_video.dart';
import 'package:unity_ads_flutter/unity_ads_flutter.dart';

enum VideoProvider {
  admob,
  facebook,
  mopub,
  inmobi,
  unity,
  adcolony,
}
enum VideoPlatform {
  android,
  ios,
}

enum AdStatus {
  loaded,
  error,
  clicked,
  closed,
  rewarded,
  opened,
  play_error,
  ssv_success,
  ssv_failed,
  iap_event,
  expired,
  left_app,
}

enum _EventHandlationType {
  load,
  show,
  both,
}
typedef AdEventHandler = Function(
  AdStatus event, {
  String currency,
  double amount,
  dynamic playBackError,
});

class VideoConfiguration {
  final VideoPlatform platform;
  final VideoProvider videoProvider;
  final String accountId;
  final Map<dynamic, String> adIds;
  final String deviceTestId;
  Duration waitTimeBetweenLoadFails;

  VideoConfiguration(
      {@required this.videoProvider,
      @required this.platform,
      @required this.adIds,
      @required this.accountId,
      this.deviceTestId,
      this.waitTimeBetweenLoadFails}) {
    if (waitTimeBetweenLoadFails == null) {
      waitTimeBetweenLoadFails = Duration(seconds: 10);
    }
  }
}

class UnityHandler extends UnityAdsListener {
  final Function eventHandler;
  final Function getAdIdentifier;
  UnityHandler(
    this.eventHandler,
    this.getAdIdentifier,
  );
  @override
  void onUnityAdsError(UnityAdsError error, String message) {
    if (error == UnityAdsError.failed_to_load) {
      eventHandler(
        adIdentifier: getAdIdentifier(message),
        type: _EventHandlationType.load,
        arguments: 3,
      );
    }
    if (error == UnityAdsError.not_initialized ||
        error == UnityAdsError.ad_blocker_detected ||
        error == UnityAdsError.initialize_failed) {
      RewardedVideoMediation._canLoadUnityAds = false;
    }
    if (error == UnityAdsError.show_error ||
        error == UnityAdsError.video_player_error) {}
  }

  @override
  void onUnityAdsFinish(String placementId, FinishState result) {
    eventHandler(
      adIdentifier: getAdIdentifier(placementId),
      type: _EventHandlationType.show,
      arguments: {
        'event': result == FinishState.completed
            ? AdStatus.rewarded
            : result == FinishState.skipped
                ? AdStatus.closed
                : AdStatus.play_error,
        'data': {},
      },
    );

    if (result == FinishState.completed) {
      eventHandler(
        adIdentifier: getAdIdentifier(placementId),
        type: _EventHandlationType.show,
        arguments: {
          'event': AdStatus.closed,
          'data': {},
        },
      );
    }
  }

  @override
  void onUnityAdsReady(String placementId) {
    eventHandler(
      adIdentifier: getAdIdentifier(placementId),
      type: _EventHandlationType.load,
      arguments: null,
    );
  }

  @override
  void onUnityAdsStart(String placementId) {
    eventHandler(
      adIdentifier: getAdIdentifier(placementId),
      type: _EventHandlationType.show,
      arguments: {
        'event': AdStatus.opened,
        'data': {},
      },
    );
  }
}

class _AdLoadConfig {
  final bool alwaysLoad;
  final int maxFailure;
  int _currentFailurecount = 0;

  _AdLoadConfig({
    this.alwaysLoad,
    this.maxFailure,
  });

  void success() {
    _currentFailurecount = 0;
  }

  void failed() {
    _currentFailurecount++;
  }

  bool isEligible() => _currentFailurecount <= (maxFailure ?? 1);
}

class RewardedVideoMediation {
  static final _allowedSwitchStates = [
    AdStatus.closed,
    AdStatus.error,
    AdStatus.expired,
  ];

  static VideoProvider _provider;
  static VideoProvider _switchProvider;
  static bool _testMode;
  static List<VideoConfiguration> _configs = [];
  static Map<dynamic, AdStatus> _states = {};
  static Map<dynamic, AdEventHandler> _handlers = {};
  static Map<dynamic, _AdLoadConfig> _loadmetadata = {};
  static Map<dynamic, int> _mopubListeners = {};
  static bool _canLoadUnityAds = true;

  static VideoConfiguration getProviderConfig() {
    return _configs.firstWhere(
      (element) => element.videoProvider == _provider,
      orElse: () => null,
    );
  }

  static void _handleAdLoadFailed(dynamic adIdentifier) {
    _states[adIdentifier] = AdStatus.error;
    _doSwitch();
    _handlers[adIdentifier]?.call(AdStatus.error);
    _loadmetadata[adIdentifier]?.failed();
    if ((_loadmetadata[adIdentifier]?.alwaysLoad ?? false == true) &&
        (_loadmetadata[adIdentifier]?.isEligible() ?? false)) {
      Future.delayed(
        getProviderConfig().waitTimeBetweenLoadFails,
        () {
          load(adIdentifier);
        },
      );
    }
  }

  static void _handleAdLoadSuccess(dynamic adIdentifier) {
    _loadmetadata[adIdentifier]?.success();
    _states[adIdentifier] = AdStatus.loaded;
    _doSwitch();
    _handlers[adIdentifier]?.call(AdStatus.loaded);
  }

  static void _handleAdShownEvents(
      dynamic adIdentifier, AdStatus event, dynamic arguments) {
    _states[adIdentifier] = event;
    _doSwitch();
    try {
      _handlers[adIdentifier]?.call(
        event,
        currency: arguments['currency'],
        amount: ((arguments['amount'] ?? 0) as num).toDouble(),
        playBackError: arguments['playError'],
      );
    } catch (err) {
      print('ad state argument error : $err');
      _handlers[adIdentifier]?.call(
        event,
        currency: null,
        amount: null,
        playBackError: null,
      );
    }
  }

  static AdStatus _decipherevent(dynamic event) {
    String eventString = event?.toString()?.toLowerCase();
    if (eventString == null) {
      print('conflict happened : expected an event but got null');
      return null;
    }
    if (eventString.contains('iap')) return AdStatus.iap_event;
    if (eventString.contains('expiring')) return AdStatus.expired;
    if (eventString.contains('leftapplication')) return AdStatus.left_app;
    if (eventString.contains('success') ||
        eventString.contains('requestfilled')) return AdStatus.loaded;
    if (eventString.contains('failure') ||
        eventString.contains('requestnotfilled')) return AdStatus.error;
    if (eventString.contains('playbackerror')) return AdStatus.play_error;

    return eventString.contains('ssv_failed')
        ? AdStatus.ssv_failed
        : eventString.contains('ssv_success')
            ? AdStatus.ssv_success
            : eventString.contains('clicked')
                ? AdStatus.clicked
                : eventString.contains('closed')
                    ? AdStatus.closed
                    : eventString.contains('play')
                        ? AdStatus.play_error
                        : (eventString.contains('opened') ||
                                eventString.contains('started'))
                            ? AdStatus.opened
                            : AdStatus.rewarded;
  }

  static void _doSwitch() {
    if (_switchProvider != null &&
        _states.values
            .every((element) => _allowedSwitchStates.contains(element))) {
      _switchVideoProvider();
    }
  }

  static void _eventHandler({
    dynamic adIdentifier,
    _EventHandlationType type,
    dynamic arguments,
  }) {
    if (type == _EventHandlationType.load) {
      arguments == null
          ? _handleAdLoadSuccess(adIdentifier)
          : _handleAdLoadFailed(adIdentifier);
    }
    if (type == _EventHandlationType.show) {
      AdStatus event = _decipherevent(arguments['event']);
      _handleAdShownEvents(adIdentifier, event, arguments['data']);
    }
    if (type == _EventHandlationType.both) {
      AdStatus event = _decipherevent(arguments['event']);
      if (event == AdStatus.loaded) {
        _handleAdLoadSuccess(adIdentifier);
      } else if (event == AdStatus.error) {
        _handleAdLoadFailed(adIdentifier);
      } else {
        _handleAdShownEvents(adIdentifier, event, arguments['data']);
      }
    }
  }

  ///Swichiting is not in the moment
  ///
  ///
  static void switchVideoProvider(VideoProvider provider) {
    _switchProvider = provider;
    assert(_switchProvider != null);
  }

  static void _switchVideoProvider() {
    assert(_switchProvider != null);
    _provider = _switchProvider;
    _states.clear();
    _loadmetadata.clear();
    _switchProvider = null;
  }

  static AdStatus getAdStatus(dynamic adIdentifier) {
    return _states[adIdentifier];
  }

  static bool _validatePlatform(VideoPlatform platform) {
    return Platform.isAndroid
        ? platform == VideoPlatform.android
        : platform == VideoPlatform.ios;
  }

  static _validateConfig(VideoConfiguration config) {
    return;
    if (_testMode) {
      assert(config.videoProvider != null && config.platform != null,
          'Null video provider or platform in configs');
      assert(
          config.videoProvider == VideoProvider.admob &&
              (config.deviceTestId != null && config.deviceTestId.isNotEmpty),
          'when in test mode for admob, you should pass a device test id\nto get test id\ndisable test mode and pass your production adIds\ndevice test id can be obtained from logs');
      /*  assert(
          config.videoProvider == VideoProvider.facebook &&
              (config.deviceTestId == null || config.deviceTestId.isEmpty),
          'when in test mode for facebook, you should pass a device test id\nto get test id\ndisable test mode and pass your production adIds\ndevice test id can be obtained from logs'); */
    } else {
      assert(config.videoProvider != null && config.platform != null,
          'Null video provider or platform in configs');
      assert(config.adIds != null && config.adIds.isNotEmpty,
          'adIds is null or empty');
      for (var adId in config.adIds.values) {
        assert(adId != null && adId.isNotEmpty, 'Null or empty adId');
      }
    }
  }

  static Future<Map<VideoProvider, bool>> initialize({
    @required List<VideoConfiguration> providersConfig,
    VideoProvider defaultProvider = VideoProvider.admob,
    bool testMode = false,
  }) async {
    _provider = defaultProvider ?? _provider;
    assert(providersConfig != null && providersConfig.isNotEmpty);
    _configs = providersConfig;
    _testMode = testMode;
    _provider = defaultProvider;
    bool isProvided = false;
    Map<VideoProvider, bool> outcome = {};
    try {
      for (var provider in providersConfig) {
        _validateConfig(provider);
        if (!_validatePlatform(provider.platform)) continue;
        if (defaultProvider == provider.videoProvider) {
          isProvided = true;
        }
        if (provider.videoProvider == VideoProvider.admob) {
          bool result = await AdmobFirebaseNewApis.init(
            testDevices: testMode ? [provider.deviceTestId] : null,
          );
          outcome[VideoProvider.admob] = result;
        }
        if (provider.videoProvider == VideoProvider.facebook) {
          bool result = await FacebookRewardedVideo.init(
            testDevices: testMode ? [provider.deviceTestId] : null,
          );
          outcome[VideoProvider.facebook] = result;
        }
        if (provider.videoProvider == VideoProvider.inmobi) {
          bool result = await InmobiRewardedVideo.init(
            provider.accountId,
            testDevices: testMode ? [] : null,
          );
          outcome[VideoProvider.inmobi] = result;
        }
        if (provider.videoProvider == VideoProvider.mopub) {
          bool result = await FlutterMopub.initilize(
            adUnitId: testMode
                ? FlutterMopub.testAdUnitId
                : provider.adIds.values.first,
          );
          outcome[VideoProvider.mopub] = result;
        }
        if (provider.videoProvider == VideoProvider.unity) {
          bool result = true;
          await UnityAdsFlutter.initialize(
              Platform.isAndroid ? provider.accountId : null,
              Platform.isIOS ? provider.accountId : null,
              UnityHandler(_eventHandler, _getAdIdentifier),
              testMode);

          outcome[VideoProvider.unity] = result;
        }

        if (provider.videoProvider == VideoProvider.adcolony) {
          bool result = true;
          await AdColony.init(
            AdColonyOptions(
              provider.accountId,
              '0',
              provider.adIds.values.toList(),
            ),
          );
          outcome[VideoProvider.adcolony] = result;
        }
      }
      if (!isProvided) {
        throw '$defaultProvider was not in your config';
      }
    } catch (err) {
      print('RewardedVideoMediation : Initialize error\n$err');
    }
    return outcome;
  }

  static String _getAdId(dynamic adIdentifier) {
    String adId;
    VideoPlatform plat =
        Platform.isAndroid ? VideoPlatform.android : VideoPlatform.ios;
    for (var element in _configs) {
      if (plat == element.platform && _provider == element.videoProvider) {
        adId = element.adIds[adIdentifier];
        break;
      }
    }
    assert(adId != null, 'couldn\'t find adId with identfier $adIdentifier');
    return adId;
  }

  static dynamic _getAdIdentifier(String adId) {
    dynamic adIdentifier;
    inner:
    for (var conf in _configs) {
      for (var key in conf.adIds.keys) {
        if (conf.adIds[key] == adId) {
          adIdentifier = key;
          break inner;
        }
      }
    }
    assert(adIdentifier != null, 'couldn\'t find adIdentifier with adId $adId');
    return adIdentifier;
  }

  static setAdStateHandler(dynamic adIdentifier, AdEventHandler handler) {
    _getAdId(adIdentifier);
    _handlers[adIdentifier] = handler;
  }

  static Future<bool> load(
    dynamic adIdentifier, {
    bool alwaysLoad,
    int maxContinuesFailure,
  }) async {
    String adId = _getAdId(adIdentifier);
    if (alwaysLoad != null || maxContinuesFailure != null)
      _loadmetadata[adIdentifier] = _AdLoadConfig(
        alwaysLoad: alwaysLoad,
        maxFailure: maxContinuesFailure,
      );
    if (_provider == VideoProvider.adcolony) {
      await AdColony.request(
        adId,
        (listener, zoneId) => zoneId == adId
            ? _eventHandler(
                adIdentifier: adIdentifier,
                arguments: {
                  'event': listener,
                },
                type: _EventHandlationType.both,
              )
            : null,
      );
      return true;
    }
    if (_provider == VideoProvider.unity) {
      if (_canLoadUnityAds) {
        await UnityAdsFlutter.load(
          adId,
        );
      }
      return _canLoadUnityAds;
    }
    if (_provider == VideoProvider.mopub) {
      if (_mopubListeners[adIdentifier] != null) {
        FlutterMopub.rewardedVideoAdInstance
            .removeRewardedVideoListener(_mopubListeners[adIdentifier]);
      }
      int id = FlutterMopub.rewardedVideoAdInstance.addRewardedVideoListener(
        listener: (event, argumnets) {
          List<String> adUnitId = [];
          String expected = _getAdId(adIdentifier);
          if (argumnets['adUnitId'] != null)
            adUnitId.add(argumnets['adUnitId']);
          if (argumnets['adUnitIds'] != null)
            adUnitId.addAll(argumnets['adUnitIds'].cast<String>());
          if (adUnitId.contains(expected))
            _eventHandler(
                adIdentifier: adIdentifier,
                arguments: {
                  'event': event,
                  'data': argumnets,
                },
                type: _EventHandlationType.both);
        },
      );
      _mopubListeners[adIdentifier] = id;
      return await FlutterMopub.rewardedVideoAdInstance.load(adUnitId: adId) >=
          0;
    }
    if (_provider == VideoProvider.admob) {
      var ad = await AdmobFirebaseNewApis.manager.create(adId);
      ad.load(
        (error) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.load,
          arguments: error,
        ),
      );
      return true;
    }
    if (_provider == VideoProvider.facebook) {
      var ad = await FacebookRewardedVideo.manager.create(adId);
      ad.load(
        (error) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.load,
          arguments: error,
        ),
      );
      return true;
    }
    if (_provider == VideoProvider.inmobi) {
      var ad = await InmobiRewardedVideo.manager.create(adId);
      ad.load(
        (error) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.load,
          arguments: error,
        ),
      );
      return true;
    }
    return false;
  }

  static Future<bool> show(dynamic adIdentifier,
      {String customData, String userId}) async {
    if (_provider != VideoProvider.unity)
      assert(_states[adIdentifier] == AdStatus.loaded,
          'You must load an ad before showing it.(provider:$_provider)');
    else
      assert(await UnityAdsFlutter.isReady(_getAdId(adIdentifier)),
          'You must load an ad before showing it.(provider:$_provider)');
    String adId = _getAdId(adIdentifier);
    if (_provider == VideoProvider.adcolony) {
      await AdColony.show(adId);
      return true;
    }
    if (_provider == VideoProvider.unity) {
      await UnityAdsFlutter.show(adId);
      return true;
    }
    if (_provider == VideoProvider.mopub) {
      String data;
      if (customData != null) {
        data = customData;
      }
      if (userId != null) {
        data = data == null ? userId : (data + "_" + userId);
      }
      return await FlutterMopub.rewardedVideoAdInstance
              .show(adUnitId: adId, customData: data) >=
          0;
    }
    if (_provider == VideoProvider.admob) {
      var ad = await AdmobFirebaseNewApis.manager.create(adId);
      assert(ad.isReady);
      ad.show(
        (event, {amount, playError, type}) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.show,
          arguments: {
            'event': event,
            'data': {
              'currency': type,
              'amount': amount,
              'playError': playError,
            },
          },
        ),
        customData: customData,
        userId: userId,
      );
      return true;
    }
    if (_provider == VideoProvider.facebook) {
      var ad = await FacebookRewardedVideo.manager.create(adId);
      assert(ad.isReady);
      ad.show(
        (event, {amount, playError, type}) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.show,
          arguments: {
            'event': event,
            'data': {
              'currency': type,
              'amount': amount,
              'playError': playError,
            },
          },
        ),
        customData: customData,
        userId: userId,
      );
      return true;
    }
    if (_provider == VideoProvider.inmobi) {
      var ad = await InmobiRewardedVideo.manager.create(adId);
      assert(ad.isReady);
      ad.show(
        (event, {amount, playError, type}) => _eventHandler(
          adIdentifier: adIdentifier,
          type: _EventHandlationType.show,
          arguments: {
            'event': event,
            'data': {
              'currency': type,
              'amount': amount,
              'playError': playError,
            },
          },
        ),
        customData: customData,
        userId: userId,
      );
      return true;
    }
    return false;
  }
}
